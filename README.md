# konsole-tab-switch
Search through your Konsole tabs by title and switch to the chosen one

Jump to your Konsole tabs by fuzzy-searching your sessions' titles from within the command-line.

## Rationale

As KDE users may have noticed, there's currently no built-in way of searching through the open tabs in Konsole, and switch to the chosen one.

This has been bothering people for [quite](https://www.reddit.com/r/kde/comments/f2st5i/shortcut_to_switch_to_a_specific_konsole_tab/) [some](https://forum.kde.org/viewtopic.php?f=227&t=133386) [time](https://debianforum.de/forum/viewtopic.php?t=178041), since the [related issue](https://bugs.kde.org/show_bug.cgi?id=298775) has been filled in 2012.

Until it's properly fixed, this repository contains a working solution. 
Consider it a temporary hack, but at least, it does the job.

## Usage

![demo.gif](https://invent.kde.org/ngirard/konsole-tab-switch/-/raw/master/demo.gif)


## Installing

**Dependencies**

Being a Bash script, `konsole-tab-switch` has no dependencies but [Fzf](https://github.com/junegunn/fzf) and `dbus-send`.

**Downloading & installing**

```sh
sudo curl -L "https://invent.kde.org/ngirard/konsole-tab-switch/-/raw/master/konsole-tab-switch" -o /usr/local/bin/konsole-tab-switch
sudo chmod +x /usr/local/bin/konsole-tab-switch
```

On Debian-based systems, you might want to take advantage of the package I built using [Nfpm](https://github.com/goreleaser/nfpm):

```sh
curl -s "https://invent.kde.org/ngirard/konsole-tab-switch/-/raw/master/konsole-tab-switch-linux_noarch.deb"
sudo dpkg -i ./konsole-tab-switch-linux_noarch.deb
```

**Integration to your environment**

Since `konsole-tab-switch` needs to be invoked from within the terminal, I'd suggest to add either an alias or a key-binding to your shell configuration. For instance, when using Bash:

* in order to alias it to `ks`, add
  ```sh
  alias ks='konsole-tab-switch'
  ```
* in order to bind `Ctrl+t` to `konsole-tab-switch`, add
  ```sh
  bind -x '"\C-t": konsole-tab-switch'
  ```

If you prefer [Skim](https://github.com/lotabout/skim) over `Fzf`, simply add
```sh
export FZF='sk'
```
to your configuration.


