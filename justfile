export name := "konsole-tab-switch"
export version := "0.4.0"
set shell := ["bash", "-c"]

debfile := name+"-linux_noarch.deb"

deb:
  nfpm pkg -f <(envsubst '${name} ${version}' < nfpm.yaml) -t "{{debfile}}"

install:
  #!/usr/bin/env bash
  [[ ! -f "{{debfile}}" ]] && just install
  sudo dpkg -i {{debfile}}

check:
  shellcheck konsole-tab-switch

demo_gif:
  gifski -Q 50 demo.mp4 --output demo.gif